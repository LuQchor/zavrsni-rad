package com.reactlibrary;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.ReadableArray;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Scalar;
import org.opencv.core.Point;
import org.opencv.core.CvType;
import org.opencv.core.Rect;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.android.Utils;

import android.util.Log;
import android.util.Base64;
import android.net.Uri;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;

public class RNOpenCvLibraryModule extends ReactContextBaseJavaModule {

    private final ReactApplicationContext reactContext;

    public RNOpenCvLibraryModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
    }

    @Override
    public String getName() {
        return "RNOpenCvLibrary";
    }

    @ReactMethod
    public void preProcessing(String imageUri, int sudokuDimensions, Callback callback) {
        try {
            Uri uri = Uri.parse(imageUri);
            String imagePath = uri.getPath();

            Mat img = Imgcodecs.imread(imagePath);

            if (img.empty()) {
                Log.e("RNOpenCvLibrary", "Failed to load image.");
                callback.invoke("Failed to load image.");
                return;
            }

            /* Log.i("RNOpenCvLibrary", "Image loaded successfully."); */

            // Image processing
            Mat imgGrayed = new Mat();
            Imgproc.cvtColor(img, imgGrayed, Imgproc.COLOR_BGR2GRAY);

            Mat imgBlured = new Mat();
            Size size = new Size(5, 5);
            Imgproc.GaussianBlur(imgGrayed, imgBlured, size, 1);

            Mat imgTreshed = new Mat();
            Imgproc.adaptiveThreshold(imgBlured, imgTreshed, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C,
                    Imgproc.THRESH_BINARY_INV, 13, 3);

            List<MatOfPoint> contours = new ArrayList<>();
            Mat hierarchy = new Mat();
            Imgproc.findContours(imgTreshed, contours, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);

            MatOfPoint biggestContour = biggestContour(contours);

            /*
             * //Comparator variable used for debugging
             * MatOfPoint biggest = biggestContour(contours);
             */

            if (biggestContour != null) {

                /*
                 * //Loging information for debugging
                 * Log.i("findRoi", "Biggest contour area: " + Imgproc.contourArea(biggest));
                 * MatOfPoint2f biggestPoints = reorder(new MatOfPoint2f(biggest.toArray()));
                 * Log.i("findRoi", "Reordered points: " +
                 * Arrays.toString(biggestPoints.toArray()));
                 */

                Mat imgWarped = findRoi(imgTreshed, Arrays.asList(biggestContour), imgTreshed.cols(),
                        imgTreshed.rows());

                Imgproc.resize(imgWarped, imgWarped, new Size(100 * sudokuDimensions, 100 * sudokuDimensions));
                imgWarped = removeLines(imgWarped, sudokuDimensions);
                Mat kernel = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(4, 4));
                Imgproc.morphologyEx(imgWarped, imgWarped, Imgproc.MORPH_CLOSE, kernel, new Point(-1, -1), 1);

                // Convert Mat to a bitmap for the final result
                Bitmap resultBitmap = Bitmap.createBitmap(imgWarped.cols(), imgWarped.rows(), Bitmap.Config.ARGB_8888);
                Utils.matToBitmap(imgWarped, resultBitmap);

                // Save the processed image as a file
                File outputDir = getReactApplicationContext().getCacheDir();
                String processedImageFileName = "processed_image" + ".jpg";
                File processedImageFile = new File(outputDir, processedImageFileName);
                processedImageFile.createNewFile();

                FileOutputStream outputStream = new FileOutputStream(processedImageFile);
                resultBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
                outputStream.close();

                /*
                 * Log.i("findRoi", "imgWarped dimensions: " + imgWarped.cols() + " x " +
                 * imgWarped.rows());
                 */

                // Return the URI of the saved image
                callback.invoke(Uri.fromFile(processedImageFile).toString());
                /* Log.i("RNOpenCvLibrary", "Image processing completed."); */
            } else {
                callback.invoke("No suitable contour found.");
            }
        } catch (Exception e) {
            Log.e("RNOpenCvLibrary", "Error processing image: " + e.getMessage());
            callback.invoke("Error processing image: " + e.getMessage());
        }
    }

    @ReactMethod
    public void splitImageIntoBoxes(String imageUri, int sudokuDimensions, Callback callback) {
        try {
            Uri uri = Uri.parse(imageUri);
            String imagePath = uri.getPath();

            // Define the directory where cached boxes are stored
            File cacheDir = getReactApplicationContext().getCacheDir();

            Mat img = Imgcodecs.imread(imagePath);

            if (img.empty()) {
                Log.e("RNOpenCvLibrary", "Failed to load image.");
                callback.invoke("Failed to load image.");
                return;
            }

            List<Mat> boxes = splitInBoxes(img, sudokuDimensions);
            List<String> boxUris = new ArrayList<>();

            // Convert each box to a bitmap and save it as a file
            for (int i = 0; i < boxes.size(); i++) {
                Mat box = boxes.get(i);

                Mat kernel2 = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3, 3));
                Imgproc.erode(box, box, kernel2);
                Imgproc.dilate(box, box, kernel2);

                Imgproc.resize(box, box, new Size(50, 50));

                Bitmap boxBitmap = Bitmap.createBitmap(box.cols(), box.rows(), Bitmap.Config.ARGB_8888);
                Utils.matToBitmap(box, boxBitmap);

                String boxFileName = "box_" + i + ".jpg";
                File boxFile = new File(cacheDir, boxFileName);
                boxFile.createNewFile();

                FileOutputStream outputStream = new FileOutputStream(boxFile);
                boxBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
                outputStream.close();

                boxUris.add(Uri.fromFile(boxFile).toString());
                int width = box.cols();
                int height = box.rows();
                /*
                 * Log.i("RNOpenCvLibrary", "Box " + i + " Dimensions (Width x Height): " +
                 * width + " x " + height);
                 */
            }
            WritableArray uriArray = Arguments.fromList(boxUris);

            // Return the URIs of boxes
            callback.invoke(uriArray);
        } catch (Exception e) {
            Log.e("RNOpenCvLibrary", "Error splitting image into boxes: " + e.getMessage());
            callback.invoke("Error splitting image into boxes: " + e.getMessage());
        }
    }

    @ReactMethod
    public void drawSolutions(String imageUri, int sudokuDimensions, ReadableArray solvedSudoku, Callback callback) {
        try {
            Uri uri = Uri.parse(imageUri);
            String imagePath = uri.getPath();

            Mat img = Imgcodecs.imread(imagePath);

            if (img.empty()) {
                Log.e("RNOpenCvLibrary", "Failed to load image.");
                callback.invoke("Failed to load image.");
                return;
            }

            int boxWidthHeight = img.cols() / sudokuDimensions;

            for (int i = 0; i < sudokuDimensions; i++) {
                for (int j = 0; j < sudokuDimensions; j++) {
                    int value = solvedSudoku.getInt(i * sudokuDimensions + j);

                    if (value != 0) {
                        int x = j * boxWidthHeight + boxWidthHeight / 3;
                        int y = (int) (i * boxWidthHeight + boxWidthHeight / 1.3);

                        // Put the text on the image
                        Imgproc.putText(img, String.valueOf(value), new Point(x, y),
                                Imgproc.FONT_HERSHEY_SIMPLEX, 2, new Scalar(0, 255, 0), 2);
                    }
                }
            }

            Bitmap resultBitmap = Bitmap.createBitmap(img.cols(), img.rows(), Bitmap.Config.ARGB_8888);
            Utils.matToBitmap(img, resultBitmap);

            File outputDir = getReactApplicationContext().getCacheDir();
            String modifiedImageFileName = "modified_image_" + ".jpg";
            File modifiedImageFile = new File(outputDir, modifiedImageFileName);
            modifiedImageFile.createNewFile();

            FileOutputStream outputStream = new FileOutputStream(modifiedImageFile);
            resultBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            outputStream.close();

            callback.invoke(Uri.fromFile(modifiedImageFile).toString());
        } catch (Exception e) {
            Log.e("RNOpenCvLibrary", "Error drawing solutions on image: " + e.getMessage());
            callback.invoke("Error drawing solutions on image: " + e.getMessage());
        }
    }

    // @ReactMethod
    // public void clearCache() {
    // try {
    // File cacheDir = getReactApplicationContext().getCacheDir();
    // if (cacheDir.exists()) {
    // File[] files = cacheDir.listFiles();

    // if (files != null) {
    // for (File file : files) {
    // file.delete();
    // }
    // }
    // Log.i("RNOpenCvLibrary", "Cache cleared successfully.");
    // } else {
    // Log.i("RNOpenCvLibrary", "Cache directory does not exist.");
    // }
    // } catch (Exception e) {
    // Log.e("RNOpenCvLibrary", "Error clearing cache: " + e.getMessage());
    // }
    // }

    private MatOfPoint biggestContour(List<MatOfPoint> contours) {
        MatOfPoint biggest = null;
        double maxArea = 0;

        for (MatOfPoint contour : contours) {
            double area = Imgproc.contourArea(contour);
            /* Log.i("RNOpenCvLibrary", "Contour area: " + area); */

            double perimeter = Imgproc.arcLength(new MatOfPoint2f(contour.toArray()), true);
            /* Log.i("RNOpenCvLibrary", "Contour perimeter: " + perimeter); */

            // Check if the contour meets criteria
            if (area > 50 && perimeter > 0) {
                MatOfPoint2f approx = new MatOfPoint2f();
                Imgproc.approxPolyDP(new MatOfPoint2f(contour.toArray()), approx, 0.02 * perimeter, true);
                int vertices = approx.toList().size();
                /* Log.i("RNOpenCvLibrary", "Number of vertices: " + vertices); */

                if (area > maxArea && vertices == 4) {
                    biggest = contour;
                    maxArea = area;
                }
            }
        }

        return biggest;
    }

    private MatOfPoint2f reorder(MatOfPoint2f myPoints) {
        Point[] points = myPoints.toArray();
        Point[] myPointsNew = new Point[4];

        double[] sums = new double[points.length];
        double[] diffs = new double[points.length];

        // Calculate the sums and differences for each point
        for (int i = 0; i < points.length; i++) {
            sums[i] = points[i].x + points[i].y;
            diffs[i] = points[i].y - points[i].x;
        }

        // Find the indices of the minimum and maximum sums and differences
        int minSumIndex = findIndexOfMin(sums);
        int maxSumIndex = findIndexOfMax(sums);
        int minDiffIndex = findIndexOfMin(diffs);
        int maxDiffIndex = findIndexOfMax(diffs);

        // Reorder the points based on the calculated indices
        myPointsNew[0] = points[minSumIndex];
        myPointsNew[1] = points[minDiffIndex];
        myPointsNew[2] = points[maxDiffIndex];
        myPointsNew[3] = points[maxSumIndex];

        MatOfPoint2f myPointsNewMat = new MatOfPoint2f();
        myPointsNewMat.fromArray(myPointsNew);

        return myPointsNewMat;
    }

    private int findIndexOfMin(double[] arr) {
        int minIndex = 0;
        double minValue = arr[0];

        for (int i = 1; i < arr.length; i++) {
            if (arr[i] < minValue) {
                minValue = arr[i];
                minIndex = i;
            }
        }

        return minIndex;
    }

    private int findIndexOfMax(double[] arr) {
        int maxIndex = 0;
        double maxValue = arr[0];

        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > maxValue) {
                maxValue = arr[i];
                maxIndex = i;
            }
        }

        return maxIndex;
    }

    private Mat findRoi(Mat img, List<MatOfPoint> contours, int widthImg, int heightImg) {
        MatOfPoint biggest = biggestContour(contours);
        if (biggest != null) {

            MatOfPoint2f biggestPoints = reorder(new MatOfPoint2f(biggest.toArray()));
            // Reordered corners of biggest contour
            MatOfPoint2f roiCorners1 = biggestPoints;
            // Destignation points where the corners of roiCorners1 should be mapped after
            // perspective transformation
            MatOfPoint2f roiCorners2 = new MatOfPoint2f(new Point(0, 0), new Point(widthImg, 0),
                    new Point(0, heightImg), new Point(widthImg, heightImg));
            Mat imgCroppedSudoku = Imgproc.getPerspectiveTransform(roiCorners1, roiCorners2);
            Mat imgWarped = new Mat();
            Imgproc.warpPerspective(img, imgWarped, imgCroppedSudoku, new Size(widthImg, heightImg));
            /*
             * Log.i("findRoi", "ROI Corners1: " + Arrays.toString(roiCorners1.toArray()));
             * Log.i("findRoi", "ROI Corners2: " + Arrays.toString(roiCorners2.toArray()));
             */
            return imgWarped;
        } else {
            return null;
        }
    }

    private Mat removeLines(Mat imgWarped, int sudokuDimensions) {
        Mat lines = new Mat();

        int minLineLength = (int) (Math.min(imgWarped.cols(), imgWarped.rows()) / sudokuDimensions * 0.8);

        // Detecting lines
        Imgproc.HoughLinesP(imgWarped, lines, 1, Math.PI / 180, 50, minLineLength, 10);

        // Go through all detected lines and draw black over them
        for (int x = 0; x < lines.rows(); x++) {
            double[] vec = lines.get(x, 0);
            double x1 = vec[0], y1 = vec[1], x2 = vec[2], y2 = vec[3];
            Point start = new Point(x1, y1);
            Point end = new Point(x2, y2);

            // Drawing a black line over detected area
            Imgproc.line(imgWarped, start, end, new Scalar(0, 0, 0), 3);
        }

        return imgWarped;
    }

    private List<Mat> splitInBoxes(Mat img, int sudokuDimensions) {
        int rows = sudokuDimensions;
        int cols = sudokuDimensions;
        int boxWidthHeight = img.cols() / cols;
        List<Mat> boxes = new ArrayList<>();

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                // ROI of the current box
                Rect roi = new Rect(j * boxWidthHeight, i * boxWidthHeight, boxWidthHeight, boxWidthHeight);
                // Save current box
                Mat box = new Mat(img, roi).clone();
                boxes.add(box);
            }
        }

        return boxes;
    }

}
