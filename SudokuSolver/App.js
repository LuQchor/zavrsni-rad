import { NavigationContainer } from "@react-navigation/native";
import AppNavigator from "./app/screens/navigation";
import { enableScreens } from "react-native-screens";
enableScreens();

export default function App() {
  return (
    <NavigationContainer>
      <AppNavigator />
    </NavigationContainer>
  );
}
