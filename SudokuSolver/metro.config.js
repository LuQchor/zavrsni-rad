const { getDefaultConfig } = require("expo/metro-config");

module.exports = async () => {
  const defaultConfig = await getDefaultConfig(__dirname);
  const { resolver } = defaultConfig;

  return {
    resolver: {
      ...resolver,
      assetExts: [...resolver.assetExts, "bin"],
    },
  };
};
