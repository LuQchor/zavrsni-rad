import { bundleResourceIO } from "@tensorflow/tfjs-react-native";
import * as tf from "@tensorflow/tfjs";
import * as jpeg from "jpeg-js";
import * as FileSystem from "expo-file-system";

async function loadModel() {
  try {
    await tf.ready();
    /* console.log(tf.getBackend()); */
    const modelJson = require("../model/model.json");
    const modelWeights = require("../model/group1-shard.bin");
    const customModel = await tf.loadLayersModel(
      bundleResourceIO(modelJson, modelWeights)
    );
    /* console.log("Model loaded successfully"); */
    return customModel;
  } catch (error) {
    console.error("Error loading model:", error);
    return null;
  }
}

async function imageToGrayscaleTensorFromURI(uri) {
  try {
    // Read the image as a base64 encoded string
    const base64ImageData = await FileSystem.readAsStringAsync(uri, {
      encoding: FileSystem.EncodingType.Base64,
    });

    // Decode the base64 data into raw binary data
    const rawImageData = tf.util.encodeString(base64ImageData, "base64").buffer;

    const { width, height, data } = jpeg.decode(rawImageData, {
      useTArray: true,
    });

    /* console.log("Image loaded:", width, "x", height); */

    // Convert the RGBA image data to grayscale 'float32' data type and normalize data [0, 1]
    const grayscaleData = new Float32Array(width * height);
    for (let i = 0; i < width * height; i++) {
      const offset = i * 4;
      grayscaleData[i] =
        (data[offset] + data[offset + 1] + data[offset + 2]) / (3 * 255);
    }

    const tensor = tf.tensor(grayscaleData, [1, height, width, 1], "float32");

    return tensor;
  } catch (error) {
    console.error("Error loading and grayscaling local image:", error);
    throw error;
  }
}

async function modelPredict(model, boxes, sudokuDimensions) {
  if (model && boxes.length > 0) {
    try {
      const boxPredictions = [];
      const whitelist = Array.from(
        { length: sudokuDimensions + 1 },
        (_, i) => i
      );

      // Empty 2D array to represent the Sudoku grid
      const sudokuGrid = Array.from({ length: sudokuDimensions }, () =>
        Array(sudokuDimensions).fill(0)
      );

      for (const boxUri of boxes) {
        const boxTensor = await imageToGrayscaleTensorFromURI(boxUri);

        const predictions = model.predict(boxTensor);

        // Convert the predictions to a JavaScript array
        const predictionsArray = await predictions.array();

        // Determine the predicted class for the current box
        // Keep only whitelisted classes with probability > 0.9
        const predictedClass = predictionsArray.map((prediction) => {
          const maxProbability = Math.max(...prediction);
          if (maxProbability > 0.9) {
            const classIndex = prediction.indexOf(maxProbability);
            return whitelist.includes(classIndex) ? classIndex : 0;
          } else {
            return 0;
          }
        });

        boxPredictions.push(predictedClass);

        // Clean up resources for the current box when done
        boxTensor.dispose();
        predictions.dispose();
      }

      for (let row = 0; row < sudokuDimensions; row++) {
        for (let col = 0; col < sudokuDimensions; col++) {
          sudokuGrid[row][col] = parseInt(
            boxPredictions[row * sudokuDimensions + col]
          );
        }
      }
      /* console.log("Sudoku Grid Predictions:", sudokuGrid); */
      return sudokuGrid;
    } catch (error) {
      console.error("Error making predictions:", error);
    }
  } else {
    console.error("Model or boxes are not available.");
  }
}

function subtractArrays(array1, array2) {
  const numRows = array1.length;
  const numCols = array1[0].length;

  const result = [];

  for (let i = 0; i < numRows; i++) {
    const row = [];
    for (let j = 0; j < numCols; j++) {
      row.push(array1[i][j] - array2[i][j]);
    }
    result.push(row);
  }

  return result;
}

export {
  loadModel,
  imageToGrayscaleTensorFromURI,
  modelPredict,
  subtractArrays,
};
