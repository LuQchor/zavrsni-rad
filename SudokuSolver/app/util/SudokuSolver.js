function solveSudoku(matrixToSolve, size) {
  const empty = findEmpty(matrixToSolve, size);
  if (!empty) {
    // Return copy
    return matrixToSolve.map((row) => [...row]);
  } else {
    const [row, col] = empty;

    for (let num = 1; num <= size; num++) {
      if (isValidMove(matrixToSolve, num, row, col, size)) {
        // Create a copy matrix, so the data in original matrix is unchanged
        const matrixCopy = matrixToSolve.map((row) => [...row]);
        matrixCopy[row][col] = num;

        const solution = solveSudoku(matrixCopy, size);
        if (solution) {
          return solution;
        }
      }
    }
  }
  return null;
}

function isValidMove(matrixToSolve, num, row, col, size) {
  for (let i = 0; i < size; i++) {
    if (matrixToSolve[row][i] == num || matrixToSolve[i][col] == num) {
      return false;
    }
  }

  const boxSizeX = Math.ceil(Math.sqrt(size));
  const boxSizeY = Math.floor(Math.sqrt(size));
  const boxX = Math.floor(col / boxSizeX) * boxSizeX;
  const boxY = Math.floor(row / boxSizeY) * boxSizeY;

  for (let i = 0; i < boxSizeY; i++) {
    for (let j = 0; j < boxSizeX; j++) {
      if (matrixToSolve[boxY + i][boxX + j] == num) {
        return false;
      }
    }
  }
  return true;
}

function findEmpty(matrixToSolve, size) {
  for (let i = 0; i < size; i++) {
    for (let j = 0; j < size; j++) {
      if (matrixToSolve[i][j] == 0) {
        return [i, j];
      }
    }
  }
  return null;
}

export { solveSudoku };
