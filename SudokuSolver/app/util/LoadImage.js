import * as ImagePicker from "expo-image-picker";

export async function pickImage() {
  const result = await ImagePicker.launchImageLibraryAsync({
    mediaTypes: ImagePicker.MediaTypeOptions.Images,
    /* allowsEditing: true, */
    quality: 1,
  });

  if (result.canceled) {
    console.log("User canceled image picker");
  }

  return result.assets ? result.assets[0].uri : null;
}

export async function takePicture() {
  const result = await ImagePicker.launchCameraAsync({
    mediaTypes: ImagePicker.MediaTypeOptions.Images,
    /* allowsEditing: true, */
    quality: 1,
  });

  if (result.canceled) {
    console.log("User canceled camera");
  }

  return result.assets ? result.assets[0].uri : null;
}
