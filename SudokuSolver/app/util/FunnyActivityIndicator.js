import React, { useState, useEffect } from "react";
import {
  View,
  ActivityIndicator,
  Text,
  StyleSheet,
  Dimensions,
} from "react-native";
const { width, height } = Dimensions.get("window");

const funnyMessages = [
  "Convincing the hamster in the server wheel to run a bit faster...",
  "Teaching unicorns how to dance the Macarena...",
  "Summoning magical pixies to cast a spell of swift computation...",
  "Counting the stars in the cloud, one by one...",
  "Hitchhiking on a comet for faster processing power...",
  "Charging up the flux capacitor for time-traveling computations...",
  "Negotiating with the gremlins in the machine for cooperation...",
  "Translating ancient hieroglyphics into modern JavaScript...",
  "Asking the CPU to speak slower so humans can keep up...",
  "Sending carrier pigeons to deliver data packets faster...",
  "Encouraging the server to do the cha-cha for quicker responses...",
  "Dusting off the abacus for some old-school calculations...",
  "Instructing the computer to think really, really hard...",
  "Convincing the bits to march in a synchronized binary parade...",
  "Training rubber ducks to assist in debugging sessions...",
  "Conducting a symphony of beep-boops for computational harmony...",
  "Persuading algorithms to play hide and seek with bugs...",
  "Synchronizing with the time-space API to avoid temporal disruptions...",
  "Encouraging servers to do yoga for better flexibility...",
  "Teaching quantum mechanics to the coffee machine...",
  "Bribing the code with cookies to run smoother...",
  "Encouraging computer bugs to become feature butterflies...",
  "Encouraging pixels to socialize and create vibrant images...",
  "Teaching error messages to apologize for their mistakes...",
];

const FunnyActivityIndicator = () => {
  const [currentMessage, setCurrentMessage] = useState(
    funnyMessages[Math.floor(Math.random() * funnyMessages.length)]
  );

  useEffect(() => {
    const interval = setInterval(() => {
      setCurrentMessage(
        funnyMessages[Math.floor(Math.random() * funnyMessages.length)]
      );
    }, 5000);
    return () => clearInterval(interval);
  }, []);

  return (
    <View style={styles.overlay}>
      <ActivityIndicator size={Math.min(height, width) * 0.3} color={"white"} />
      <Text style={styles.message}>{currentMessage}</Text>
    </View>
  );
};

const styles = {
  overlay: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: "rgba(0, 0, 0, 0.75)",
    justifyContent: "center",
    alignItems: "center",
  },
  message: {
    textAlign: "center",
    marginTop: 10,
    color: "white",
    paddingHorizontal: width * 0.05,
  },
};

export default FunnyActivityIndicator;
