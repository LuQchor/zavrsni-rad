import React, { useState, useEffect } from "react";
import {
  SafeAreaView,
  StyleSheet,
  View,
  Dimensions,
  Image,
  ScrollView,
} from "react-native";
import AntDesign from "react-native-vector-icons/AntDesign";
import Octicons from "react-native-vector-icons/Octicons";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import AppButton from "../components/AppButton";
import colors from "../config/colors";
import { pickImage, takePicture } from "../util/LoadImage";
import { loadModel, modelPredict, subtractArrays } from "../util/OcrFunctions";
import { solveSudoku } from "../util/SudokuSolver";
import FunnyActivityIndicator from "../util/FunnyActivityIndicator";
const { width, height } = Dimensions.get("window");

import { NativeModules, PermissionsAndroid } from "react-native";
const { RNOpenCvLibrary } = NativeModules;

function UploadImageScreen({ route }) {
  const sudokuDimensions = route.params.sudokuDimensions;
  const [selectedImage, setSelectedImage] = useState(null);
  const [processedImage, setProcessedImage] = useState(null);
  const [boxes, setBoxes] = useState([]);
  const [model, setModel] = useState(null);
  const [modelLoaded, setModelLoaded] = useState(false);
  const [solving, setSolving] = useState(false);

  useEffect(() => {
    if (!modelLoaded) {
      (async () => {
        const customModel = await loadModel();
        setModel(customModel);
        setModelLoaded(true);
      })();
    }

    if (processedImage) {
      RNOpenCvLibrary.splitImageIntoBoxes(
        processedImage,
        sudokuDimensions,
        (boxUris) => {
          if (Array.isArray(boxUris)) {
            setBoxes(boxUris);
          } else {
            console.log("Could not load box Array");
          }
        }
      );
    }
  }, [processedImage, sudokuDimensions, modelLoaded]);

  const handleSolveSudoku = () => {
    setSolving(true);
    if (processedImage && model) {
      modelPredict(model, boxes, sudokuDimensions)
        .then((sudokuGrid) => {
          const solvedSudoku = solveSudoku(sudokuGrid, sudokuDimensions);
          /* console.log("Solved Sudoku:", solvedSudoku); */
          const differenceArray = subtractArrays(solvedSudoku, sudokuGrid);
          /* console.log("Difference Array:", differenceArray); */
          // Draw solutions on the processed image
          const timestamp = new Date().getTime();
          RNOpenCvLibrary.drawSolutions(
            processedImage,
            sudokuDimensions,
            differenceArray.flat(),
            (modifiedImageUri) => {
              setProcessedImage(modifiedImageUri + `?timestamp=${timestamp}`);
            }
          );
          setSolving(false);
        })
        .catch((error) => {
          console.error("Error predicting Sudoku:", error);
          setSolving(false);
        });
    } else {
      console.error("Model, processed image, or boxes are not available.");
      setSolving(false);
    }
  };

  const handleTakePicture = async () => {
    try {
      const capturedImage = await takePicture();
      if (capturedImage) {
        setSelectedImage(capturedImage);
        const timestamp = new Date().getTime();
        RNOpenCvLibrary.preProcessing(
          capturedImage,
          sudokuDimensions,
          (processedImageUri) => {
            setProcessedImage(processedImageUri + `?timestamp=${timestamp}`);
          }
        );
      }
    } catch (error) {
      console.error("Error taking a picture:", error);
    }
  };

  const handleImagePick = async () => {
    try {
      const pickedImage = await pickImage();
      if (pickedImage) {
        setSelectedImage(pickedImage);
        // Timestamp is being used to force check every time if the image has changed
        const timestamp = new Date().getTime();
        RNOpenCvLibrary.preProcessing(
          pickedImage,
          sudokuDimensions,
          (processedImageUri) => {
            setProcessedImage(`${processedImageUri}?timestamp=${timestamp}`);
          }
        );
      }
    } catch (error) {
      console.error("Error picking an image:", error);
    }
  };

  return (
    <View style={styles.viewcolour}>
      <SafeAreaView style={styles.container}>
        {!processedImage ? (
          <View style={styles.placeholderView}></View>
        ) : (
          <Image
            source={{ uri: processedImage }}
            style={styles.processedImage}
          />
        )}
        <AppButton
          title="Take a picture"
          style={styles.button}
          IconComponent={AntDesign}
          iconName="camera"
          iconColor="black"
          onPress={handleTakePicture}
        />
        <AppButton
          title="Load from device"
          style={styles.button}
          IconComponent={Octicons}
          iconName="file-directory"
          iconColor="black"
          onPress={handleImagePick}
        />
        {processedImage && (
          <AppButton
            title="Predict Numbers"
            style={styles.button}
            IconComponent={MaterialIcons}
            iconName={"batch-prediction"}
            iconColor="black"
            onPress={handleSolveSudoku}
          ></AppButton>
        )}
        {solving && <FunnyActivityIndicator />}
      </SafeAreaView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    paddingTop: height * 0.04,
  },
  viewcolour: {
    flex: 1,
    backgroundColor: colors.backgroundcolor,
  },
  button: {
    marginTop: height * 0.02,
  },
  selectedImage: {
    width: 100,
    height: 100,
    resizeMode: "contain",
  },
  processedImage: {
    marginTop: height * 0.1,
    marginBottom: height * 0.1,
    height: height * 0.4,
    aspectRatio: 1,
  },
  placeholderView: {
    marginTop: height * 0.1,
    marginBottom: height * 0.1,
    height: height * 0.4,
    aspectRatio: 1,
    backgroundColor: "transparent",
  },
  overlay: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: "rgba(0, 0, 0, 0.75)",
    justifyContent: "center",
    alignItems: "center",
  },
});

export default UploadImageScreen;
