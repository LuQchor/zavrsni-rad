import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  Dimensions,
  Image,
} from "react-native";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import AppButton from "../components/AppButton";
import colors from "../config/colors";
import { useFonts, Montserrat_400Regular } from "@expo-google-fonts/montserrat";

const { height, width } = Dimensions.get("window");

function HomeScreen({ navigation }) {
  return (
    <View style={styles.viewcolour}>
      <SafeAreaView style={styles.container}>
        <Text style={styles.title}>SudokuSolver</Text>
        <Image
          style={[styles.image]}
          source={require("../assets/images/puzzle.png")}
        />
        <AppButton
          title="Choose Dimensions"
          onPress={() => navigation.navigate("Sudoku")}
          style={styles.button}
          IconComponent={SimpleLineIcons}
          iconName="options-vertical"
          iconColor="black"
        />
      </SafeAreaView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    paddingTop: height * 0.07,
    paddingBottom: height * 0.03,
  },
  viewcolour: {
    flex: 1,
    backgroundColor: colors.backgroundcolor,
  },
  title: {
    fontSize: Math.min(height, width) * 0.11,
    color: "black",
    fontFamily: "Montserrat_400Regular",
    letterSpacing: 2,
    textTransform: "uppercase",
  },
  image: {
    marginTop: height * 0.05,
    height: "50%",
    width: "80%",
    alignSelf: "center",
    marginBottom: height * 0.12,
  },
  button: {
    marginTop: height * 0.01,
  },
});

export default HomeScreen;
