import {
  SafeAreaView,
  StyleSheet,
  View,
  Dimensions,
  Image,
} from "react-native";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";

import AppButton from "../components/AppButton";
import colors from "../config/colors";

const { width, height } = Dimensions.get("window");

function SudokuScreen({ navigation }) {
  const handleButtonClick = (sudokuDimensions) => {
    navigation.navigate("UploadImage", { sudokuDimensions });
  };

  return (
    <View style={styles.viewcolour}>
      <SafeAreaView style={styles.container}>
        <Image
          source={require("../assets/images/sudokuGif.gif")}
          style={styles.gif}
        />
        <AppButton
          title="4x4 Newbie"
          onPress={() => handleButtonClick(4)}
          style={styles.button}
          IconComponent={FontAwesome5}
          iconName="brain"
          iconColor="red"
        />
        <AppButton
          title="6x6 Junior"
          onPress={() => handleButtonClick(6)}
          style={styles.button}
          IconComponent={FontAwesome5}
          iconName="brain"
          iconColor="#CD7F32"
        />
        <AppButton
          title="9x9 Classic"
          onPress={() => handleButtonClick(9)}
          style={styles.button}
          IconComponent={FontAwesome5}
          iconName="brain"
          iconColor="gold"
        />
        <AppButton
          title="12x12 Pro"
          onPress={() => handleButtonClick(12)}
          style={styles.button}
          IconComponent={FontAwesome5}
          iconName="brain"
          iconColor="cyan"
        />
        <AppButton
          title="16x16 Champion"
          onPress={() => handleButtonClick(16)}
          style={styles.button}
          IconComponent={FontAwesome5}
          iconName="brain"
          iconColor="blue"
        />
      </SafeAreaView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center",
    paddingBottom: height * 0.05,
  },
  viewcolour: {
    flex: 1,
    backgroundColor: colors.backgroundcolor,
  },
  button: {
    marginTop: height * 0.01,
  },
  gif: {
    height: height * 0.3,
    aspectRatio: 1,
    marginBottom: height * 0.1,
  },
});

export default SudokuScreen;
