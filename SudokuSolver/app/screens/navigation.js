import { createStackNavigator } from "@react-navigation/stack";
import { Platform } from "react-native";

import HomeScreen from "./HomeScreen";
import SudokuScreen from "./SudokuScreen";
import UploadImageScreen from "./UploadImageScreen";

const Stack = createStackNavigator();

function AppNavigator() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Home"
        component={HomeScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Sudoku"
        component={SudokuScreen}
        options={{
          headerShown: shouldShowHeader(),
          headerTitle: "Sudoku",
        }}
      />
      <Stack.Screen
        name="UploadImage"
        component={UploadImageScreen}
        options={{
          headerShown: shouldShowHeader(),
          headerTitle: "UploadImage",
        }}
      />
    </Stack.Navigator>
  );
}

function shouldShowHeader() {
  return Platform.OS === "ios" ? true : false;
}

export default AppNavigator;
