Setup:


1) Clone the Git repository:

This YouTube video might help you if the next steps are unclear. Keep in mind that not everything in the video  needs to be done, just the first few steps: https://www.youtube.com/watch?v=ELYL2fFi1SE

2) Download OpenCV 4.8.0 Android version:
   
3) Create a sub-folder outside of the SudokuSolver folder and name it lib. Inside of it, place the OpenCV-android-sdk that you downloaded.

4) Run the ./downloadAndInsertOpenCV.sh script.

5) Open Android Studio and mount the android folder that is located within the SudokuSolver directory:
   Wait for Gradle to build (it takes quite some time).

6) Open Visual Studio Code and mount the SudokuSolver directory:
   Open the terminal and write npx react-native start.
   When the prompt shows, click on a to launch on Android.
    
Link for Data.rar which I used for training my model: https://drive.google.com/file/d/1AsLS8J2YIi4AuRpc_cdD4QF2VqLI2Tvs/view?usp=sharing

Link for android apk version of this application: https://gitlab.com/LuQchor/zavrsni-rad/-/tree/BuiltApk?ref_type=heads

Bug fixes that might help you
C:\Users\user\AppData\Local\Android\Sdk\cmdline-tools\10.0 -> Set it to the latest version, otherwise React Native will not recognize the Android SDK at all.


my versions:
node v18.16.0
npm 9.5.1
react-native-cli: 2.0.1
react-native: 0.72.6
expo 6.3.10
android studio Giraffe
gradle JDK jbr-17 JetBrains Runtime version 17.0.6
cmdline-tools 10.0